import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  selected?:Article;
  article:Article[] = [];

  constructor(private opService: ArticleService) { }

  ngOnInit(): void {
    this.opService.getAll().subscribe(data => this.article = data);
  }

  fetchOne() {
    this.opService.getById(2).subscribe(data => this.selected = data);
  }

}

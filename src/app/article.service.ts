import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from './entities';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Article[]>('http://localhost:8080/api/article');
  }

  getById(id:number) {
    return this.http.get<Article>('http://localhost:8080/api/article/'+id);
  }

  add(article:Article) {
    return this.http.post<Article>('http://localhost:8080/api/article', article);
  }

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/article/'+id);
  }
}


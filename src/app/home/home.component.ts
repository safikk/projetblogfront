import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  selected?:Article;
  articles:Article[] = [];

  constructor(private service: ArticleService) { }

  ngOnInit(): void {
    this.service.getAll().subscribe(data => this.articles = data);
  }
    fetchArticle(id:number) {
      this.service.getById(id).subscribe(data => this.selected = data);
    }

    delete(id:number) {
      this.service.delete(id).subscribe();
      this.removeFromList(id);
    }
  
    removeFromList(id: number) {
      this.articles.forEach((article, article_index) => {
        if(article.id == id) {
          this.articles.splice(article_index, 1);
        }
      });
    }

}

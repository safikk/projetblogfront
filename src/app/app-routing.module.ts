import { Component, ComponentRef, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ArticleComponent } from './article/article.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes: Routes = [
  // { path: '', component: ArticleComponent },
  { path: 'header', component: HeaderComponent},
  { path: '', component: HomeComponent},
  { path: 'add-article', component: AddComponent},
  { path: 'article/:id', component: SingleArticleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

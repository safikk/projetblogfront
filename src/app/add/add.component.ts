import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  article:Article = {
    label: '',
    id: 0,
    date: '',
    text: '',
    image: ''
  };

  constructor(private service:ArticleService, private router:Router) { }

  ngOnInit(): void {
  }

  addArticle() {
    this.service.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

}

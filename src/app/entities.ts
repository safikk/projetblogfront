
export interface Comments {
    id:number;
    texte:string;
    date:string;
}

export interface Article {
    id:number;
    label:string;
    date:string;
    text:string;
    image:string;
}
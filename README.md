# ProjetBlog

## Description

- Cette application est un blog où tous les visiteurs peuvent y rédiger et déposer un article de leur choix avec un titre, un texte et la date sans avoir besoin de créer un compte. (Pour le moment, on ne peut pas faire de commentaire sur les articles ni déposer d'image).


- Le blog est codé en Spring Boot pour la partie Back et Angular pour la partie Front.



## Backend :

- Entity: Classes ArticleEntity et CommentsEntity avec les constructors et les getter/setter

- Repository: ArticleRepository et CommentsRepository pour faire les requêtes vers la base de données.

- Controller: ArticlesControleur et CommentsController pour réceptionner les requêtes HTTP.



## Frontend :

Les 3 pages sont constiuées de :

- Un addComponent

- Un articleComponent

- Un headerComponent

- Un homeComponent

- Un single-articleComponent




# Visuel

Ci-dessous, la maquette du blog :

- [La maquette](/Maquette%20projet%20blog.png) : les 3 pages en 1 seule image.



## Les difficultées :

- J'avais prévue au début du projet qu'on puisse y ajouter des commantaires et des images (j'ai même coder la partie Back dans ce sens) mais j'ai finalement abandonné lorsque j'ai codé la partie Front par manque de temps...

- Je suis conscient que mon travail Angular est polué par plusieurs dossiers qu'a moitié opérationnel pour certains (article et header par exemple, le html n'est pas opérationnel mais le .ts fonctionne, j'ai mis le html en commantaire). Bref, j'ai préféré laissé comme ça car au final le blog fonctionne...



## Futur Mise à Jour :

- (Améliorer la partie Angular les dossier article et header).

- Proposer aux utilisateurs la posibilité d'ajouter des images à leur article ainsi que de laisser des commantaires.

- Afficher seulement le début du texte de l'article (dans les cards des articles de la page d'acceuil) sur 2 ou 3 lignes maxi et ne consulter l'article en entier qu'en cliquant sur le boutton "voir l'article".

- Améliorer la partie front pour la rendre plus attrayant : Améliorer le design en géneral, faire une "vraie" page d'acceuil, ajouter un footer, ...



